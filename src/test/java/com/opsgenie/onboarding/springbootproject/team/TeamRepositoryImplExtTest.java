package com.opsgenie.onboarding.springbootproject.team;

import com.opsgenie.onboarding.springbootproject.accessor.DynamoDBAccessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TeamRepositoryImplExtTest {

    private TeamRepository repository;

    @Before
    public void setUp() {
        repository = new TeamRepositoryImpl(new DynamoDBAccessor());
    }

    @After
    public void tearDown() {
        repository = null;
    }

    @Test
    public void todo() throws Exception {

    }
}
