package com.opsgenie.onboarding.springbootproject.team;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.opsgenie.onboarding.springbootproject.exception.EntityNotFoundException;
import com.opsgenie.onboarding.springbootproject.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */

@RunWith(SpringRunner.class)
@WebMvcTest(TeamController.class)
public class TeamControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamServiceImpl teamService;

    Team expectedTeam;
    String expectedTeamJson;
    String expectedDtoJson;
    Exception exception;

    @Before
    public void setUp() throws JsonProcessingException {
        expectedTeam = new Team("1", "ankara", null);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        expectedTeamJson = objectWriter.writeValueAsString(expectedTeam);
        expectedDtoJson = objectWriter.writeValueAsString(expectedTeam.toDTO());

        exception = new EntityNotFoundException("Team not found with identifier [" + expectedTeam.getId() + "]");
    }

    @Test
    //@Ignore
    public void listAllEmpty() throws Exception {
        when(teamService.listAll()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/teams/listAll"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    //@Ignore
    public void listAll() throws Exception {
        List<Team> expectedTeams = Arrays.asList(expectedTeam,expectedTeam);
        when(teamService.listAll()).thenReturn(expectedTeams);

        List<TeamDTO> dtoList = new ArrayList<>();
        for (Team team : expectedTeams) {
            dtoList.add(team.toDTO());
        }
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String expectedJson = objectWriter.writeValueAsString(dtoList);

        mockMvc.perform(get("/teams/listAll"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));

        verify(teamService, times(1)).listAll();
    }

    @Test
    //@Ignore
    public void getTeam() throws Exception {
        when(teamService.getTeam(expectedTeam.getId())).thenReturn(expectedTeam);
        mockMvc.perform(get("/teams/get/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedTeamJson));
    }

    @Test
    public void getNonExistedTeam () throws Exception {
        when(teamService.getTeam(any())).thenThrow(exception);
        mockMvc.perform(get("/teams/get/1"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(exception.getMessage()));
    }

    @Test
    //@Ignore
    public void createTeam() throws Exception {
        when(teamService.createTeam(any())).thenReturn(expectedTeam);
        mockMvc.perform(post("/teams/create")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(expectedDtoJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedTeamJson));
    }

    @Test
    public void createExistedTeam () throws Exception {
        when(teamService.createTeam(any())).thenThrow(exception);
        mockMvc.perform(post("/teams/create")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(expectedDtoJson))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(exception.getMessage()));
    }

    @Test
    public void deleteTeam () throws Exception {
        doNothing().when(teamService).deleteTeam(expectedTeam.getId());
        mockMvc.perform(delete("/teams/delete/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("The item has been deleted."));
    }

    @Test
    public void deleteNonExistedTeam () throws Exception {
        doThrow(exception).when(teamService).deleteTeam(any());
        mockMvc.perform(delete("/teams/delete/1"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(exception.getMessage()));
    }

    @Test
    public void updateTeam () throws Exception {
        when(teamService.updateTeam(any())).thenReturn(expectedTeam);
        mockMvc.perform(put("/teams/update")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(expectedDtoJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedTeamJson));
    }

    @Test
    public void updateNonExistedTeam () throws Exception {
        when(teamService.updateTeam(any())).thenThrow(exception);
        mockMvc.perform(put("/teams/update")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(expectedDtoJson))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(exception.getMessage()));
    }
}

