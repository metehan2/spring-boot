package com.opsgenie.onboarding.springbootproject.team;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.opsgenie.onboarding.springbootproject.accessor.DynamoDBAccessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TeamRepositoryImplTest {

    @Mock
    private DynamoDBAccessor accessor;

    private TeamRepository repository;

    private Team expected;
    private DateFormat df;
    private Item item;

    @Before
    public void setUp() {
        repository = new TeamRepositoryImpl(accessor);
        expected = new Team("id1", "a", null);
        expected.setTime(new Date());
        df = new SimpleDateFormat();
        item = new Item()
                .withPrimaryKey("Id", expected.getId())
                .withString("Name", expected.getName())
                //.withStringSet("UserIds", expected.getUserIds())
                .withString("CreateDate", df.format(expected.getCreateDate()))
                .withString("UpdateDate", df.format(expected.getUpdateDate()));
    }

    @After
    public void tearDown() {
        repository = null;
    }

    @Test
    public void listTeams() {

    }

    @Test
    public void getTeam() {
        when(accessor.getItem("Id", expected.getId())).thenReturn(item);
        final Team team = repository.getTeam(expected.getId());

        assertThat(team, is(equalTo(expected)));
        verify(accessor, times(1)).getItem("Id", expected.getId());
    }
}
