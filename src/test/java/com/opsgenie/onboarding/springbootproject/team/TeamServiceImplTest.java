package com.opsgenie.onboarding.springbootproject.team;

import com.opsgenie.onboarding.springbootproject.exception.EntityNotFoundException;
import com.opsgenie.onboarding.springbootproject.sqs.SqsCommunicator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceImplTest {

    private TeamService teamService;

    @Mock
    private TeamRepository teamRepository;

    @Mock
    private SqsCommunicator sqsCommunicator;

    private Team expected;

    @Before
    public void setUp() {
        teamService = new TeamServiceImpl(teamRepository, sqsCommunicator);
        expected = new Team("id1", "a", new HashSet<>(Arrays.asList("x","y")));
    }

    @After
    public void tearDown() {
        teamRepository = null;
        teamService = null;
    }


    @Test
    public void listAllTeams() {

        final List<Team> expected = Arrays.asList(new Team("id1", "a", new HashSet<>(Arrays.asList("x","y"))  ),
                new Team("id2", "b", new HashSet<>(Arrays.asList("k","m"))  ));
        when(teamRepository.listAll()).thenReturn(expected);
        final List<Team> teams = teamService.listAll();

        assertThat(teams, is(equalTo(expected)));
        verify(teamRepository, times(1)).listAll();
    }

    @Test
    public void getTeam () {
        when(teamRepository.getTeam(expected.getId())).thenReturn(expected);
        final Team team = teamService.getTeam(expected.getId());
        assertThat(team, is(equalTo(expected)));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTeamException () {
        teamService.getTeam(expected.getId());
    }

    @Test
    public void createTeam () {
        when(teamRepository.createTeam(expected)).thenReturn(expected);
        final Team team = teamService.createTeam(expected);
        assertThat(team, is(equalTo(expected)));
    }

    @Test
    public void deleteTeam () {
        doNothing().when(teamRepository).deleteTeam(expected.getId());
        teamService.deleteTeam(expected.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteTeamException () {
        teamService.deleteTeam(expected.getId());
    }

    @Test
    public void updateTeam () {
        when(teamRepository.getTeam(expected.getId())).thenReturn(expected);
        when(teamRepository.updateTeam(expected)).thenReturn(expected);
        final Team team = teamService.updateTeam(expected);
        assertThat(team, is(equalTo(expected)));
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateTeamException () {
        teamService.updateTeam(expected);
    }

}
