package com.opsgenie.onboarding.springbootproject.team;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
public class TeamDTO {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("userIds")
    private Set<String> userIds;

    public TeamDTO() {}

    public TeamDTO(String id, String name, Set<String> userIds) {
        this.id = id;
        this.name = name;
        this.userIds =userIds;
    }

    @JsonIgnore
    public static TeamDTO from(Team team) {
        return new TeamDTO(team.getId(),team.getName(),team.getUserIds());
    }

    @JsonIgnore
    public Team toTeam() {
        return new Team(id,name,userIds);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<String> userIds) {
        this.userIds = userIds;
    }
}
