package com.opsgenie.onboarding.springbootproject.team;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.document.Item;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
@DynamoDBTable(tableName = "onboarding-team")
public class Team {

    private String id;
    private String name;
    private Set<String> userIds;
    private Date createDate;
    private Date updateDate;

    public Team(){}

    public Team(String id, String name, Set<String> userIds){
        this.id = id;
        this.name = name;
        this.userIds = userIds;
    }

    public Team(Item item){
        this.id = item.get("Id").toString();
        this.name = item.get("Name").toString();



        DateFormat format = new SimpleDateFormat();

        try {
            this.createDate = format.parse(item.get("CreateDate").toString());
            this.updateDate = format.parse(item.get("UpdateDate").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @DynamoDBIgnore
    public TeamDTO toDTO(){
        TeamDTO dto = new TeamDTO(id,name,userIds);
        return dto;
    }

    @DynamoDBIgnore
    public void copy(Team team){
        this.id = team.id;
        this.name = team.name;
        this.userIds = team.userIds;
        this.createDate = team.createDate;
        this.updateDate = team.updateDate;
    }

    @DynamoDBHashKey(attributeName = "Id")
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "Name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @DynamoDBAttribute(attributeName = "UserIds")
    public Set<String> getUserIds() {
        return userIds;
    }
    public void setUserIds(Set<String> userIds) {
        this.userIds = userIds;
    }

    @DynamoDBAttribute(attributeName = "CreateDate")
    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @DynamoDBAttribute(attributeName = "UpdateDate")
    public Date getUpdateDate() {
        return updateDate;
    }
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @DynamoDBIgnore
    public void setTime(Date time) {
        this.createDate = time;
        this.updateDate = time;
    }
}
