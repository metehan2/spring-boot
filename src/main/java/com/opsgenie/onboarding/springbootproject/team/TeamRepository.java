package com.opsgenie.onboarding.springbootproject.team;

import java.util.List;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
public interface TeamRepository {
    List<Team> listAll();

    Team getTeam(String teamId);
    Team createTeam(Team team);
    void deleteTeam(String teamId);
    Team updateTeam(Team team);
}
