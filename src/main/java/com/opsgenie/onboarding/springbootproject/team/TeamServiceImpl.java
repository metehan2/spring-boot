package com.opsgenie.onboarding.springbootproject.team;

import com.opsgenie.onboarding.springbootproject.exception.EntityNotFoundException;
import com.opsgenie.onboarding.springbootproject.sqs.SqsCommunicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
@Service
public class TeamServiceImpl implements TeamService {

    private final TeamRepository teamRepository;
    private final SqsCommunicator sqsCommunicator;

    @Autowired
    TeamServiceImpl(TeamRepository teamRepository, SqsCommunicator sqsCommunicator){
        this.teamRepository = teamRepository;
        this.sqsCommunicator = sqsCommunicator;
    }

    @Override
    public List<Team> listAll() {
        return teamRepository.listAll();
    }

    @Override
    public Team getTeam(String teamId){
        Team team = teamRepository.getTeam(teamId);

        if (team == null) {
            throw new EntityNotFoundException("Team not found with identifier [" + teamId + "]");
        }

        return team;
    }

    @Override
    public Team createTeam(Team team){
        Team exist = teamRepository.getTeam(team.getId());

        if (exist != null){
            throw new EntityNotFoundException("Team already found with identifier [" + exist.getId() + "]");
        }

        team.setTime(new Date());
        return teamRepository.createTeam(team);
    }

    @Override
    public void deleteTeam(String teamId){
        Team team = teamRepository.getTeam(teamId);

        if (team == null){
            throw new EntityNotFoundException("Team not found with identifier [" + teamId + "]");
        }
    }

    @Override
    public Team updateTeam(Team team){
        Team exist = teamRepository.getTeam(team.getId());

        if (exist == null){
            throw new EntityNotFoundException("Team not found with identifier [" + team.getId() + "]");
        }

        team.setCreateDate(exist.getCreateDate());
        team.setUpdateDate(new Date());

        return teamRepository.updateTeam(team);
    }

    @Override
    public void assignTeamMember(){
        sqsCommunicator.getMessagesFromQueue();
    }

}
