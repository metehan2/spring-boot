package com.opsgenie.onboarding.springbootproject.team;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.opsgenie.onboarding.springbootproject.accessor.DynamoDBAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;



/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
@Repository
public class TeamRepositoryImpl implements TeamRepository {

    DynamoDBAccessor accessor;
    private final String tableName = "onboarding-team";

    @PostConstruct
    public void setup(){
        accessor.setTableName(tableName);
        accessor.setTable(accessor.getDynamoDB().getTable(tableName));
    }

    @Autowired
    public TeamRepositoryImpl(DynamoDBAccessor accessor) {
        this.accessor = accessor;
    }

    
    public List<Team> listAll() {
        List<Team> teams = new LinkedList<>();

        ItemCollection<ScanOutcome> items = accessor.scan();
        Iterator<Item> iterator = items.iterator();

        while (iterator.hasNext()) {
            Item item = iterator.next();
            teams.add(new Team(item));
        }

        return teams;
    }

    public Team getTeam(String teamId){
        Item item = accessor.getItem("Id", teamId);

        if (item == null)
            return null;

        return new Team(item);
    }

    public Team createTeam(Team team){
        DateFormat df = new SimpleDateFormat();

        Item item = new Item()
                .withPrimaryKey("Id", team.getId())
                .withString("Name", team.getName())
                //.withStringSet("UserIds", team.getUserIds())
                .withString("CreateDate", df.format(team.getCreateDate()))
                .withString("UpdateDate", df.format(team.getUpdateDate()));

        if (team.getUserIds() != null && !team.getUserIds().isEmpty()) {
            item.withStringSet("UserIds", team.getUserIds());
        }

        accessor.putItem(item);

        return getTeam(team.getId());
    }

    public void deleteTeam(String teamId){
        accessor.deleteItem("Id", teamId);
    }

    public Team updateTeam(Team team){
        DateFormat df = new SimpleDateFormat();

        Map<String, String> expressionAttributeNames = new HashMap<String, String>();
        expressionAttributeNames.put("#N", "Name");
        expressionAttributeNames.put("#UI", "UserIds");
        expressionAttributeNames.put("#CD", "CreateDate");
        expressionAttributeNames.put("#UD", "UpdateDate");

        Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
        expressionAttributeValues.put(":name", team.getName());
        expressionAttributeValues.put(":userIds", team.getUserIds());
        expressionAttributeValues.put(":createDate", df.format(team.getCreateDate()));
        expressionAttributeValues.put(":updateDate", df.format(team.getUpdateDate()));

        String updateExpression = "set #N = :name, #UI = :userIds, #CD = :createDate, #UD = :updateDate";

        accessor.updateItem(new PrimaryKey("Id",team.getId()),
                updateExpression,
                expressionAttributeNames,
                expressionAttributeValues);

        return getTeam(team.getId());
    }
}
