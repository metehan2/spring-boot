package com.opsgenie.onboarding.springbootproject.team;

import com.opsgenie.onboarding.springbootproject.exception.EntityNotFoundException;
import com.opsgenie.onboarding.springbootproject.sqs.SqsCommunicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */

@RestController
@RequestMapping("/teams")
public class TeamController {

    private final TeamService teamService;
    private final SqsCommunicator sqsCommunicator;

    @Autowired
    public TeamController(TeamService teamService, SqsCommunicator sqsCommunicator) {
        this.teamService = teamService;
        this.sqsCommunicator = sqsCommunicator;
    }

    @RequestMapping(value = "/{teamId}/assignTeamMember", method = RequestMethod.POST)
    public ResponseEntity assignTeamMember(@RequestBody List<String> userIds) {
        String ids = userIds.stream().collect(Collectors.joining(","));
        return ResponseEntity.accepted().body(sqsCommunicator.sendMessageToQueue(ids));
    }

    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    public List<TeamDTO> listAll() {
        List<TeamDTO> dtoList = new ArrayList<>();
        for (Team team : teamService.listAll()) {
            dtoList.add(team.toDTO());
        }
        return dtoList;
    }

    @RequestMapping(value = "/get/{teamId}", method = RequestMethod.GET)
    public ResponseEntity getTeam(@PathVariable String teamId) {
        Team team;
        try{
            team = teamService.getTeam(teamId);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(team);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity createTeam(@RequestBody TeamDTO dto) {
        Team team;
        try{
            team = teamService.createTeam(dto.toTeam());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(team);
    }

    @RequestMapping(value = "/delete/{teamId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteTeam(@PathVariable String teamId) {
        try{
            teamService.deleteTeam(teamId);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body("The item has been deleted.");
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity updateTeam(@RequestBody TeamDTO dto) {
        Team team;
        try{
            team = teamService.updateTeam(dto.toTeam());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(team);
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "hello";
    }

    @RequestMapping(value = "/hello2", method = RequestMethod.GET)
    public ResponseEntity hello2(){
        return ResponseEntity.status(HttpStatus.OK).body("hello2");
    }

}
