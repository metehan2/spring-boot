package com.opsgenie.onboarding.springbootproject.sqs;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author Metehan OZTURK
 * @version 20.07.2018
 */
@Component
public class SqsCommunicator {
    private final String url = "https://sqs.us-east-1.amazonaws.com/985426034680/onboarding-queue";
    private final String accessKey = "AKIAJYTQW7PVTAVQKVPQ";
    private final String secretKey = "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";
    private  final String region = "us-east-1";

    private BasicAWSCredentials credentials;
    private AmazonSQS sqs;

    @PostConstruct
    public void setup() {
        credentials = new BasicAWSCredentials(accessKey,secretKey);
        sqs = AmazonSQSClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    public List<Message> getMessagesFromQueue(){
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(url);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();

        return messages;
    }

    public String sendMessageToQueue(String message){
        SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(url)
                .withMessageBody(message)
                .withDelaySeconds(5);

        SendMessageResult messageResult =  sqs.sendMessage(sendMessageRequest);

        return messageResult.toString();
    }

    public String deleteMessageFromQueue(Message message){
        String messageRecieptHandle = message.getReceiptHandle();
        DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest(url, messageRecieptHandle);
        DeleteMessageResult messageResult = sqs.deleteMessage(deleteMessageRequest);

        return messageResult.toString();
    }

}
