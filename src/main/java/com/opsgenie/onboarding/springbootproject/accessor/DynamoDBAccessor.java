package com.opsgenie.onboarding.springbootproject.accessor;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * @author Metehan OZTURK
 * @version 19.07.2018
 */
@Component
public class DynamoDBAccessor<T> {

    private AmazonDynamoDB client;
    private DynamoDB dynamoDB;
    private String tableName;
    private Table table;

    @PostConstruct
    public void setup() {
        client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("http://localhost:3966", "us-west-2"))
                .build();
        dynamoDB = new DynamoDB(client);
    }

    public ItemCollection<ScanOutcome> scan() {
        return table.scan();
    }

    public Item getItem(String hashKeyName, String value){
        return table.getItem("Id", value);
    }

    public void putItem(Item item){
        table.putItem(item);
    }

    public void deleteItem(String hashKeyName, String value){
        table.deleteItem("Id", value);
    }

    public void updateItem(PrimaryKey key, String updateExpression, Map<String, String> expressionAttributeNames, Map<String, Object> expressionAttributeValues){

        table.updateItem(
                key,
                updateExpression,
                expressionAttributeNames,
                expressionAttributeValues);
    }

    public AmazonDynamoDB getClient() {
        return client;
    }

    public void setClient(AmazonDynamoDB client) {
        this.client = client;
    }

    public DynamoDB getDynamoDB() {
        return dynamoDB;
    }

    public void setDynamoDB(DynamoDB dynamoDB) {
        this.dynamoDB = dynamoDB;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
