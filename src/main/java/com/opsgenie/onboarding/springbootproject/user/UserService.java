package com.opsgenie.onboarding.springbootproject.user;

import java.util.List;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
public interface UserService {
    List<User> listAll();

    User getUser(String teamId);
    User createUser(User user);
    User deleteUser(String teamId);
    User updateUser(User user);
}
