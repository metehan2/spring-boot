package com.opsgenie.onboarding.springbootproject.user;

import com.opsgenie.onboarding.springbootproject.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public List<User> listAll() {
        return userRepository.listAll();
    }

    @Override
    public User getUser(String teamId){
        User user = userRepository.getTeam(teamId);

        if (user == null) {
            throw new EntityNotFoundException("Team not found with identifier [" + teamId + "]");
        }

        return user;
    }

    @Override
    public User createUser(User user){
        user.setTime(new Date());
        return userRepository.createTeam(user);
    }

    @Override
    public User deleteUser(String teamId){
        User user = userRepository.getTeam(teamId);

        if (user == null){
            throw new EntityNotFoundException("Team not found with identifier [" + teamId + "]");
        }
        return userRepository.deleteTeam(teamId);
    }

    @Override
    public User updateUser(User user){
        User oldUser = userRepository.getTeam(user.getId());

        if (oldUser == null){
            throw new EntityNotFoundException("Team not found with identifier [" + user.getId() + "]");
        }

        user.setCreateTime(oldUser.getCreateTime());
        user.setUpdateTime(new Date());

        return userRepository.updateTeam(user);
    }
}
