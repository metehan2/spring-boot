package com.opsgenie.onboarding.springbootproject.user;

import java.util.List;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
public interface UserRepository {
    List<User> listAll();

    User getTeam(String teamId);
    User createTeam(User user);
    User deleteTeam(String teamId);
    User updateTeam(User user);
}
