package com.opsgenie.onboarding.springbootproject.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
public class UserDTO {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    public UserDTO() {}

    public UserDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public User toTeam() {
        User user = new User(id,name);
        return user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
