package com.opsgenie.onboarding.springbootproject.user;

import java.util.Date;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
public class User {

    private String id;
    private String name;
    private Date createTime;
    private Date updateTime;

    public User(){
    }

    public User(String id, String name){
        this.id = id;
        this.name = name;
    }

    public UserDTO toDTO(){
        UserDTO dto = new UserDTO(id,name);
        return dto;
    }

    public void copy(User user){
        this.id = user.id;
        this.name = user.name;
        this.createTime = user.createTime;
        this.updateTime = user.updateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setTime(Date time) {
        this.createTime = time;
        this.updateTime = time;
    }
}
