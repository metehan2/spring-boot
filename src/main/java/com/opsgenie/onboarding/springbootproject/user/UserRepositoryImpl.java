package com.opsgenie.onboarding.springbootproject.user;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Metehan OZTURK
 * @version 17.07.2018
 */
@Component
public class UserRepositoryImpl implements UserRepository {

    private List<User> userList;
    
    @PostConstruct
    public void setup() {
        userList = new ArrayList<>();
    }
    
    public List<User> listAll() {
        //List<Team> teamList = new ArrayList<>();
        // teamList.add(new Team("id1", "a", new HashSet<>(Arrays.asList("x", "y"))));
        // teamList.add(new Team("id2", "b", new HashSet<>(Arrays.asList("k", "m"))));
        return userList;
    }

    public User getTeam(String teamId){
        Optional<User> team = userList.stream()
                            .filter(t -> t.getId().equals(teamId))
                            .findFirst();

        if (team.isPresent()){
            return team.get();
        }
        return null;
    }

    public User createTeam(User user){
        User exist = getTeam(user.getId());
        if (exist == null) {
            userList.add(user);
            return user;
        }
        return exist;
    }

    public User deleteTeam(String teamId){
        User user = getTeam(teamId);
        if (user != null){
            userList.remove(user);
        }
        return user;
    }

    public User updateTeam(User user){
        User exist = getTeam(user.getId());
        if (exist != null){
            exist.copy(user);
        }
        return user;
    }
}
