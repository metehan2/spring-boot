package com.opsgenie.onboarding.springbootproject.exception;

/**
 * @author Metehan OZTURK
 * @version 18.07.2018
 */
public class EntityNotFoundException extends RuntimeException{
    public EntityNotFoundException() {
        super();
    }
    public EntityNotFoundException(String message) {
        super(message);
    }
}
